package com.skicco.spring.controller;

import com.skicco.spring.entity.SkillDetails;
import com.skicco.spring.facade.SkiccoFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 */
@Controller
@RequestMapping("/skicco")
@CrossOrigin
public class SkiccoController {

    @Autowired
    SkiccoFacade skiccoFacade;

    @GetMapping(value = "/")
    public String homePage() {
        return "index";
    }

    /**
     * List of SkillDetails is passed in multiple scenarios
     * (i) When User Selects List of Skills corresponding to the user
     * (ii)when Hirer posts a project, these list of skills will be displayed, upon which Hirer selects the skills which are required
     *
     * @return List of SkillDetails
     */
    @GetMapping(value = "/retrieveSkillDetails")
    public @ResponseBody
    ResponseEntity<List<SkillDetails>> retrieveSkillDetails() {
        final List<SkillDetails> skillDetails = skiccoFacade.retrieveSkillDetails();
        return new ResponseEntity<>(skillDetails, HttpStatus.OK);
    }
}
