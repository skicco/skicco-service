package com.skicco.spring.controller;

import com.skicco.spring.dto.HirerDTO;
import com.skicco.spring.dto.HirerDetailsDTO;
import com.skicco.spring.facade.HirerFacade;
import com.skicco.spring.util.SkiccoConstants;
import com.skicco.spring.util.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author SriHarsha
 */

@RestController
@RequestMapping("/skicco")
@CrossOrigin // dev purpose to skip cors
@Slf4j
public class HirerController {
    @Autowired
    HirerFacade hirerFacade;

    @PostMapping(value = "/hirerSignup")
    public @ResponseBody
    ResponseEntity<Integer> signUp(@RequestBody HirerDTO hirer) {
        final Integer responseCode = hirerFacade.signUp(hirer);
        if (responseCode != null) {
            return new ResponseEntity<>(responseCode, HttpStatus.OK);
        }
        return null;
    }

    @GetMapping(value = "/hirerVerification/{huid}")
    public @ResponseBody
    ResponseEntity<Integer> hirerVerification(@PathVariable("huid") String huid) {
        log.info("Huid : " + huid);
        final Integer responseCode = hirerFacade.hirerVerification(huid);
        if (responseCode == 200) {
            return new ResponseEntity<>(responseCode, HttpStatus.OK);
        } else if (responseCode == 0) {
            return new ResponseEntity<>(responseCode, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }

    @PostMapping(value = "/saveHirerDetails")
    public @ResponseBody
    ResponseEntity<Integer> saveHirerDetails(@RequestHeader final HttpHeaders headers, @RequestBody HirerDetailsDTO hirerDetailsDTO) {
        String huid = hirerDetailsDTO.getHuid();
        log.info("Save hirer details for :: " + huid);
        Integer responseCode = hirerFacade.saveHirerDetails(hirerDetailsDTO);
        if (responseCode == 200) {
            return new ResponseEntity<>(responseCode, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(responseCode, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/hirerLogin")
    public @ResponseBody
    ResponseEntity<Integer> login(@RequestBody HirerDTO hirerDTO) {
        final String responseCodeUuid = hirerFacade.login(hirerDTO);
        log.info("Hirer Login....responseCodeUuid : " + responseCodeUuid + " !!!");
        if (responseCodeUuid != null) {
            final HttpHeaders responseHeaders = new HttpHeaders();
            final int responseCode = Integer.parseInt(responseCodeUuid.split("##")[0]);
            if (responseCode != 0) {
                final String huid = responseCodeUuid.split("##")[1];
                responseHeaders.set(SkiccoConstants.VALUE_HIRER, JwtTokenProvider.generateToken(huid, SkiccoConstants.VALUE_HIRER));
            }
            return new ResponseEntity<>(responseCode, responseHeaders, HttpStatus.OK);
        }
        return null;
    }

}
