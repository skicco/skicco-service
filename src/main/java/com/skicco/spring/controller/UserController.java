package com.skicco.spring.controller;

import com.skicco.spring.dto.UserDTO;
import com.skicco.spring.dto.UserDashBoardDetailsDTO;
import com.skicco.spring.dto.UserDetailsRequestDTO;
import com.skicco.spring.dto.UserSkillDTO;
import com.skicco.spring.facade.UserFacade;
import com.skicco.spring.util.SkiccoConstants;
import com.skicco.spring.util.jwt.JwtTokenProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author KRISHNA PANDU RANGA
 */
@RestController
@RequestMapping("/skicco")
@CrossOrigin // dev purpose to skip cors :) as the request is from 4200 port
@Slf4j
public class UserController {

    @Autowired
    UserFacade userFacade;

    @PostMapping(value = "/userSignup")
    public @ResponseBody
    ResponseEntity<Integer> signUp(@RequestBody UserDTO userDTO) {
        log.info("User Signup....User : " + userDTO.getName() + " !!!");
        Integer responseCode = userFacade.signUp(userDTO);
        if (responseCode != null) {
            return new ResponseEntity<>(responseCode, HttpStatus.OK);
        }
        return null;
    }

    @GetMapping(value = "/userVerification/{uuid}")
    public @ResponseBody
    ResponseEntity<Integer> userVerification(@PathVariable("uuid") String uuid) {
        log.info("Uuid : " + uuid);
        Integer responseCode = userFacade.userVerification(uuid);
        if (responseCode == 200 || responseCode == 203) {
            return new ResponseEntity<>(responseCode, HttpStatus.OK);
        } else if (responseCode == 0) {
            return new ResponseEntity<>(responseCode, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }

    @PostMapping(value = "/userlogin")
    public @ResponseBody
    ResponseEntity<Integer> login(@RequestBody UserDTO userDTO) {
        Map<String, Object> map = userFacade.login(userDTO);
        int response = (Integer) map.get("response");
        log.info("User login....responseCode : " + response + " !!!");
        final HttpHeaders responseHeaders = new HttpHeaders();
        if (response != 0) {
            UserDashBoardDetailsDTO userDashBoardDetailsDTO = (UserDashBoardDetailsDTO) map.get("user");
            final String uuid = userDashBoardDetailsDTO.getUuid();
            responseHeaders.add(SkiccoConstants.KEY_AUTHORIZATION, JwtTokenProvider.generateToken(uuid, SkiccoConstants.VALUE_SKICON));
        }
        return new ResponseEntity<>(response, responseHeaders, HttpStatus.OK);
    }

    @PostMapping(value = "/saveUserDetails")
    public @ResponseBody
    ResponseEntity<Integer> saveUserDetails(@RequestHeader final HttpHeaders headers, @RequestBody UserDetailsRequestDTO userDetailsRequestDTO) {
        log.info("saveUserDetails " + userDetailsRequestDTO);
        String uuid = userDetailsRequestDTO.getUuid();
        log.info("uuid " + uuid);
        Integer responseCode = userFacade.saveUserDetails(userDetailsRequestDTO);
        if (responseCode == 200) {
            return new ResponseEntity<>(responseCode, headers, HttpStatus.OK);
        } else if (responseCode == 0) {
            return new ResponseEntity<>(responseCode, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }

    @PostMapping(value = "/saveUserSkillDetails")
    public @ResponseBody
    ResponseEntity<Integer> saveUserSkillDetails(@RequestHeader final HttpHeaders headers, @RequestBody UserSkillDTO userSkillDTO) {
        final Map<String, Object> map = userFacade.saveUserSkillDetails(userSkillDTO);
        int response = (Integer) map.get("response");
        log.info("User login controller....responseCode : " + response + " !!!");
        if (response != 0) {
            return new ResponseEntity<>(response, headers, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(response, headers, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
