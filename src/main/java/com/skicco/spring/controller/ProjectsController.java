package com.skicco.spring.controller;

import com.skicco.spring.dto.ProjectDTO;
import com.skicco.spring.facade.ProjectFacade;
import com.skicco.spring.util.SkiccoConstants;
import com.skicco.spring.util.jwt.JwtTokenChecker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 */
@RestController
@RequestMapping("/skicco")
@CrossOrigin
@Slf4j
public class ProjectsController {

    @Autowired
    private ProjectFacade projectFacade;

    @PostMapping(value = "/postProject")
    public @ResponseBody
    ResponseEntity<String> postProject(@RequestHeader final HttpHeaders headers, @RequestBody ProjectDTO project) {
        final Integer responseCode = projectFacade.postProjectDetails(project);
        String responseMessage = "";
        if (responseCode == 200) {
            responseMessage = "Project posted successfully!!!";
            return new ResponseEntity<>(responseMessage, headers, HttpStatus.OK);
        } else if (responseCode == 0) {
            responseMessage = "There is an issue while posting a project. Please post a project after sometime!!!";
            return new ResponseEntity<>(responseMessage, headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }

    /**
     * This method inserts the details of the skiccon Id, hirer Id and project Id
     * of the project which is shown interest by Skiccon
     * <p>
     * //@param showInterest
     *
     * @return Response Object
     *//*
    @RequestMapping(value = "/showInterest", method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<Response> showInterestInProject(@RequestBody ShowInterest showInterest) {
        Response responseObj = new Response();
        Integer responseCode = projectFacade.saveShownInterestedProject(showInterest);
        responseObj.setResponseCode(responseCode);
        if (responseCode == 200) {
            responseObj.setUid(showInterest.getUuid());
            responseObj.setResponseMsg("Project Shown Interest is saved successfully!!!");
            return new ResponseEntity<Response>(responseObj, HttpStatus.OK);
        } else if (responseCode == 0) {
            responseObj.setResponseMsg("There is an issue while saving an project which is Shown Interest. Please try again later!!!");
            return new ResponseEntity<Response>(responseObj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return null;
    }*/
    @GetMapping(value = "/skicon/retrieveskilledprojects/{index}")
    public @ResponseBody
    ResponseEntity<List<ProjectDTO>> retrieveSkiconSkilledProjects(@RequestHeader final HttpHeaders headers, @PathVariable("index") final int index) {
        final String jsonWebToken = headers.get(SkiccoConstants.KEY_AUTHORIZATION).stream().findFirst().get();
        final String uuid = JwtTokenChecker.authenticate(jsonWebToken, SkiccoConstants.VALUE_SKICON);
        final List<ProjectDTO> projects = projectFacade.retrieveSkiconSkilledProjects(uuid, index);
        return new ResponseEntity<>(projects, headers, HttpStatus.OK);
    }

    @GetMapping(value = "/skicon/retrieveotherskilledprojects/{index}")
    public @ResponseBody
    ResponseEntity<List<ProjectDTO>> retrieveSkiconOtherSkilledProjects(@RequestHeader final HttpHeaders headers, @PathVariable("index") final int index) {
        final String jsonWebToken = headers.get(SkiccoConstants.KEY_AUTHORIZATION).stream().findFirst().get();
        final String uuid = JwtTokenChecker.authenticate(jsonWebToken, SkiccoConstants.VALUE_SKICON);
        final List<ProjectDTO> projects = projectFacade.retrieveSkiconOtherSkilledProjects(uuid, index);
        return new ResponseEntity<>(projects, headers, HttpStatus.OK);
    }

    /* @RequestMapping(value = "/hirer/retrieveProjects/{huid}", method = RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<Response> retrieveHirerRelatedProjects(@PathVariable("huid") String huid) {
        Response responseObj = new Response();
        Map<String, List<Project>> projects = projectFacade.retrieveHirerRelatedProjects(huid);
        responseObj.setUid(huid);
        responseObj.setResponseCode(200);
        responseObj.setResponseMsg(projects);
        return new ResponseEntity<>(responseObj, HttpStatus.OK);
    }*/
}
