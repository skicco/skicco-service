package com.skicco.spring.model;

import java.io.Serializable;

/**
 * @author KRISHNA PANDU RANGA
 */

public class SubSkill implements Serializable {

    private static final long serialVersionUID = 1L;

    //************************************************
    //Properties of Sub skills
    //************************************************

    private String subSkill;
    private float rating;

    //*********************************************
    //Constructor
    //********************************************

    public SubSkill() {
    }

    public SubSkill(String subSkill, float rating) {
        this.subSkill = subSkill;
        this.rating = rating;
    }

    //************************************************
    //Setters and Getters of Subskills
    //************************************************

    public String getSubSkill() {
        return subSkill;
    }

    public void setSubSkill(String subSkill) {
        this.subSkill = subSkill;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
