package com.skicco.spring.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Skill Details Entity is mapped to skill_details table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "skill_details")
public class SkillDetails implements Serializable {

    //****************************************************************
    // Properties of a Skill Details
    //****************************************************************
    @JsonIgnore
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "skill")
    private String skill;

    @JsonIgnore
    @Column(name = "sub_skills")
    private String subskillsStr;

    @Transient
    private List<String> subSkills;

    @Transient
    private int displayKey;


}
