package com.skicco.spring.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * User Entity is mapped to User table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "user")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class User implements Serializable {

    //****************************************************************
    // Properties of a User
    //****************************************************************

    @Id
    @Column(name = "uuid", nullable = false)
    private String uuid;

    @Column(name = "email")
    private String emailId;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String pwd;

    @Column(name = "status")
    private String status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserDetails> userDetails;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserAddress> userAddresses;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserEducation> userEducations;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserAccount> userAccounts;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserTransaction> userTransactions;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserProjectInterest> userProjectInterests;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserRating> userRatings;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserSkill> userSkills;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserProjectAllocation> userProjectAllocations;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserPreferences> userPreferences;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "uuid")
    private Set<UserPayment> userPayments;

    @Embedded
    private GenericDetails genericDetails;
}
