package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User Project Interest Entity is mapped to project_interest table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "user_project_interest")
public class UserProjectInterest implements Serializable {

    //****************************************************************
    // Properties of a User Project Interest
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_project_id")
    private int id;

    @Column(name = "status")
    private String status;

    @Column(name = "hirer_interest")
    private String hirerInterest;

    @Column(name = "user_interest")
    private String userInterest;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "uuid", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Embedded
    private GenericDetails genericDetails;
}
