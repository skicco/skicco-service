package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User Education Entity is mapped to user_education table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "user_education")
public class UserEducation implements Serializable {

    //****************************************************************
    // Properties of a User Education
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "edu_qualification")
    private String educationQualification;

    @Column(name = "edu_specialization")
    private String educationSpecialization;

    @Column(name = "year_of_compltn")
    private int yearOfCompletion;

    @Column(name = "college_name")
    private String collegeName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uuid", nullable = false)
    private User user;

    @Embedded
    private GenericDetails genericDetails;
}
