package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Project Entity is mapped to project table
 *
 * @author KRISHNA PANDU RANGA
 */
//@Data
@Getter
@Setter
@Entity
@Table(name = "project")
public class Project implements Serializable {

    //****************************************************************
    // Properties of a Project
    //****************************************************************

    @Id
    @GenericGenerator(name = "sequence_prj_id", strategy = "com.skicco.spring.util.ProjectIdGenerator")
    @GeneratedValue(generator = "sequence_prj_id")
    @Column(name = "id")
    private String id;

    @Column(name = "title")
    private String title;

    @Column(name = "number_of_resources")
    private int numberOfResources;

    @Column(name = "description", columnDefinition = "varchar(9999)")
    private String description;

    @Column(name = "skill")
    private String skill;

    @Column(name = "sub_skills")
    private String subSkills;

    @Column(name = "files_path")
    private String filesPath;

    @Column(name = "estimated_start_time")
    private String estimatedStartTime;

    @Column(name = "budget_type")
    private String budgetType;

    @Column(name = "cutoff_date_for_show_interest")
    private String cutOffDateForShowInterest;

    @Column(name = "estimated_budget")
    private String estimatedBudget;

    @Column(name = "estimated_time")
    private String estimatedTime;

    @Column(name = "number_of_vacancies")
    private int numberOfVacancies;

    @Column(name = "location_type")
    private String locationType;

    @Column(name = "pref_country")
    private String preferenceCountry;

    @Column(name = "status")
    private int status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "project_id")
    private Set<UserProjectInterest> userProjectInterests;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "project_id")
    private Set<UserProjectAllocation> userProjectAllocations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "huid", nullable = false)
    private Hirer hirer;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "project_id")
    private Set<ProjectAddress> projectAddresses;

    @Embedded
    private GenericDetails genericDetails;

}
