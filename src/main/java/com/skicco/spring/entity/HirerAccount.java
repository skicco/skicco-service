package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Hirer Account Entity is mapped to hirer_account table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "hirer_account")
public class HirerAccount implements Serializable {

    //****************************************************************
    // Properties of a Hirer Account
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "last_login")
    private String lastLogin;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "huid", nullable = false)
    private Hirer hirer;

    @Embedded
    private GenericDetails genericDetails;
}
