package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Hirer Transaction Entity is mapped to hirer_transaction table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "hirer_transaction")
public class HirerTransaction {

    //****************************************************************
    // Properties of a Hirer Transaction
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "amount")
    private long amount;

    @Column(name = "type")
    private String type;

    @Column(name = "status")
    private String status;

    @Column(name = "message")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "huid", nullable = false)
    private Hirer hirer;

    @Embedded
    private GenericDetails genericDetails;
}
