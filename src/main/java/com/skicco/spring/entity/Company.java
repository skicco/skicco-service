package com.skicco.spring.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Company entity is mapped to company table
 */

@Getter
@Setter
@Entity
@Table(name = "company")
public class Company implements Serializable {

    //****************************************************************
    // Properties of a Company
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name="name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "company_id")
    private Set<Hirer> hirer;

    @Embedded
    private GenericDetails genericDetails;

}
