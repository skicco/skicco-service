package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

/**
 * Hirer Entity is mapped to Hirer table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "hirer")
public class Hirer implements Serializable {

    //****************************************************************
    // Properties of a Hirer
    //****************************************************************

    @Id
    @Column(name = "huid", nullable = false)
    private String huid;

    @Column(name = "email")
    private String email;

    @Column(name = "name")
    private String name;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="position")
    private String position;

    @Column(name="mobileNumber")
    private String mobileNumber;

    @Column(name = "password")
    private String password;

    @Column(name = "client_type")
    private String clientType;

    @Column(name = "status")
    private String status;

    @Column(name="hirer_verification")
    private Boolean isHirerVerified;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "huid")
    private Set<Project> projects;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "huid")
    private Set<HirerAddress> hirerAddresses;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "huid")
    private Set<HirerAccount> hirerAccounts;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "huid")
    private Set<HirerTransaction> hirerTransactions;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(unique = true, name = "huid")
    private Set<HirerPayment> hirerPayments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn( name = "company_id" ,nullable = true,insertable = false, updatable = false)
    private Company company;

    @Embedded
    private GenericDetails genericDetails;

}
