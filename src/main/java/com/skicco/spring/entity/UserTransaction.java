package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * User Transaction Entity is mapped to user_transaction table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "user_transaction")
public class UserTransaction implements Serializable {

    //****************************************************************
    // Properties of a User Transaction
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "amount")
    private long amount;

    @Column(name = "type")
    private String type;

    @Column(name = "status")
    private String status;

    @Column(name = "message")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "uuid", nullable = false)
    private User user;

    @Embedded
    private GenericDetails genericDetails;

}
