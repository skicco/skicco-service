package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Project Allocation Entity is mapped to project_allocation table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "user_project_allocation")
public class UserProjectAllocation implements Serializable {

    //****************************************************************
    // Properties of a Project Allocation
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "status")
    private String status;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "uuid", nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", nullable = false)
    private Project project;

    @Embedded
    private GenericDetails genericDetails;
}
