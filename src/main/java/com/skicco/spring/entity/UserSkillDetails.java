package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
public class UserSkillDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    //Properties for Subskill
    private String skill;
    private String subSkill;
    private float rating;
}
