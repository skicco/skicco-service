package com.skicco.spring.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * This class consists of the Generic attributes
 *
 * @author KRISHNA PANDU RANGA
 */
@Embeddable
@Data
public class GenericDetails {

    //****************************************************************
    // Properties of a Generic Details
    //****************************************************************

    @Column(name = "created_by")
    private String createdBy;

    @Column(name = "modified_by")
    private String modifiedBy;

    @Column(name = "created_time")
    private String createdTime;

    @Column(name = "modified_time")
    private String modifiedTime;

}
