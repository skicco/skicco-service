package com.skicco.spring.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Hirer Payment Entity is mapped to hirer_payment table
 *
 * @author KRISHNA PANDU RANGA
 */
@Getter
@Setter
@Entity
@Table(name = "hirer_payment")
public class HirerPayment implements Serializable {

    //****************************************************************
    // Properties of a Hirer Payment
    //****************************************************************

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "cash_balance")
    private long cashBalance;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "huid", nullable = false)
    private Hirer hirer;

    @Embedded
    private GenericDetails genericDetails;

}
