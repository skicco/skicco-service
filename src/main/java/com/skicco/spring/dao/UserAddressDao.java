package com.skicco.spring.dao;

import com.skicco.spring.entity.UserAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface UserAddressDao extends JpaRepository<UserAddress, Serializable> {
}
