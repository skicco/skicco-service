package com.skicco.spring.dao;

import com.skicco.spring.entity.HirerAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author SriHarsha
 */
@Transactional
@Repository
public interface HirerAddressDao extends JpaRepository<HirerAddress, Serializable> {
}
