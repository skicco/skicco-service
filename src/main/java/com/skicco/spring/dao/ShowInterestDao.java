package com.skicco.spring.dao;

import com.skicco.spring.entity.UserProjectInterest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author KRISHNA PANDU RANGA
 */
@Transactional
@Repository
public interface ShowInterestDao extends JpaRepository<UserProjectInterest, Serializable> {
}
