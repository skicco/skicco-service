package com.skicco.spring.dao;

import com.skicco.spring.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 */
@Transactional
@Repository
public interface ProjectDao extends JpaRepository<Project, Serializable> {

    @Query(value = "select * from project p where p.status IN ?1 and p.number_of_vacancies <> 0 and p.sub_skills REGEXP ?2 ORDER BY p.id DESC LIMIT ?3,5", nativeQuery = true)
    List<Project> findSkiconProjectsByStatusAndNoOfVacancies(List<Integer> statuses, String subSkills, int index);

    @Query(value = "select * from project p where p.status IN ?1 and p.number_of_vacancies <> 0 and p.sub_skills NOT REGEXP ?2 ORDER BY p.id DESC LIMIT ?3,5", nativeQuery = true)
    List<Project> findSkiconOtherSkilledProjectsByStatusAndNoOfVacancies(List<Integer> statuses, String subSkills, int index);
}
