package com.skicco.spring.dao;

import com.skicco.spring.entity.UserEducation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface UserEducationDao extends JpaRepository<UserEducation,Serializable> {
}
