package com.skicco.spring.dao;

import com.skicco.spring.entity.UserPreferences;
import org.springframework.data.jpa.repository.JpaRepository;

import java.io.Serializable;

public interface UserPreferencesDao extends JpaRepository<UserPreferences, Serializable> {
}
