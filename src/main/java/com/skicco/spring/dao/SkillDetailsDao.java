package com.skicco.spring.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.skicco.spring.entity.SkillDetails;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
@Transactional
@Repository
public interface SkillDetailsDao extends JpaRepository<SkillDetails, Serializable>{

}
