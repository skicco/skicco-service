/**
 * 
 */
package com.skicco.spring.dao;

import com.skicco.spring.entity.Hirer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author SriHarsha
 *
 */

@Transactional
@Repository
public interface HirerDao extends JpaRepository<Hirer, Serializable> {

    Hirer findByEmail(String emailAddr);

    Hirer findHirerByEmailAndPassword(String emailAddr, String password);

    Hirer findByHuid(String huid);

    @Modifying
    @Query("update Hirer h set h.name = ?2, h.status = ?3, h.genericDetails.modifiedBy = ?2 ,h.genericDetails.modifiedTime = ?4 where h.huid = ?1")
    int updateHirerNameAndStatusByHuid(String huid,String name, String status, String modifiedTime);

    @Modifying
    @Query("update Hirer u set u.status = ?2 , u.genericDetails.modifiedBy = ?3 ,u.genericDetails.modifiedTime  = ?4 where u.huid = ?1")
    int updateStatusByUuid(String uuid, String status, String modifiedBy, String modifiedTime);

    /*@Query("select u from Hirer u where u.emailId = ?1 and u.pwd = ?2")
    Hirer findHirerByEmailAndPassword(String emailAddr, String password);*/
    /*
	@Query("select u from Hirer u where u.emailId = ?1")
	Hirer findByEmailAddress(String emailAddr);
	

	
	@Modifying
	@Query("update Hirer u set u.statusId = ?2 , u.genericDetails.modifiedBy = ?3 ,u.genericDetails.modifiedTime  = ?4 where u.huid = ?1")
	int updateStatusByhuid(String huid,String status, String modifiedBy, String modifiedTime);



	@Query("select u.name from Hirer u where u.huid = ?1")
    String findHirerNameByHuid(String huid);*/
}
