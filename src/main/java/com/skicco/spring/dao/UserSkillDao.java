package com.skicco.spring.dao;

import com.skicco.spring.entity.UserSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author JAYANT KUMAR
 *
 */
@Transactional
@Repository
public interface UserSkillDao extends JpaRepository<UserSkill, Serializable> {

    @Query("select u from UserSkill u where u.user.uuid = ?1")
    UserSkill findByUuid(String uuid);
}
