package com.skicco.spring.dao;

import com.skicco.spring.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * @author KRISHNA PANDU RANGA
 */
@Transactional
@Repository
public interface UserDao extends JpaRepository<User, Serializable> {

    @Query("select u from User u where u.emailId = ?1")
    User findByEmailId(String emailId);

    @Modifying
    @Query("update User u set u.status = ?2 , u.genericDetails.modifiedBy = ?3 ,u.genericDetails.modifiedTime  = ?4 where u.uuid = ?1")
    int updateStatusByUuid(String uuid, String status, String modifiedBy, String modifiedTime);


    @Query("select u from User u where u.emailId = ?1 and u.pwd = ?2")
    User findUserByEmailIdAndPwd(String emailId, String password);
	

	@Modifying
	@Query("update User u set u.name = ?2, u.status = ?3, u.genericDetails.modifiedBy = ?2 ,u.genericDetails.modifiedTime = ?4 where u.uuid = ?1")
	int updateUserNameAndStatusByUUID(String uuid,String name, String status, String modifiedTime);

    @Query("select u from User u where u.uuid = ?1")
    User findByUuid(String uuid);

}
