package com.skicco.spring.dao;

import com.skicco.spring.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.io.Serializable;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
@Transactional
@Repository
public interface UserDetailsDao extends JpaRepository<UserDetails, Serializable>{

}
