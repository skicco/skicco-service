package com.skicco.spring.service;

import com.skicco.spring.entity.Project;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 */
public interface ProjectService {

    Integer postProjectDetails(final Project project);

    List<Project> retrieveSkiconSkilledProjects(final String uuid, final int index);

    List<Project> retrieveSkiconOtherSkilledProjects(final String uuid, final int index);

   /* Integer saveShownInterestedProject(ShowInterest showInterest);

    Map<String, List<Project>> retrieveHirerRelatedProjects(String huid);*/
}
