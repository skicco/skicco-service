/**
 * 
 */
package com.skicco.spring.service;

import com.skicco.spring.entity.Hirer;

import java.util.Map;

/**
 * @author SriHarsha
 *
 */
public interface HirerService {

	Map<String, Object> signUp(Hirer hirer);

	Integer hirerVerification(String huid);

	Integer saveUserDetails(final Map<String, Object> hirerDetailsMap);

		String findUser(Hirer hirer);
}
