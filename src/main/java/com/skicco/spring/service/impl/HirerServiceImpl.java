package com.skicco.spring.service.impl;

import com.skicco.spring.dao.CompanyDao;
import com.skicco.spring.dao.HirerAddressDao;
import com.skicco.spring.dao.HirerDao;
import com.skicco.spring.entity.Company;
import com.skicco.spring.entity.Hirer;
import com.skicco.spring.entity.HirerAddress;
import com.skicco.spring.service.HirerService;
import com.skicco.spring.util.ResponseEnum;
import com.skicco.spring.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @author SriHarsha
 */

@Service
public class HirerServiceImpl implements HirerService {

    private static final Logger LOG = LoggerFactory.getLogger(HirerServiceImpl.class);
    private static final String STATUS_HIRER = "Hirer";
    @Autowired
    HirerDao hirerDao;
    @Autowired
    HirerAddressDao hirerAddressDao;
    @Autowired
    CompanyDao companyDao;

    @Override
    public Map<String, Object> signUp(Hirer hirer) {

        Map<String, Object> map = new HashMap<>();
        LOG.info("SignUp Service...!!!");
        String emailId = hirer.getEmail();
        /*
         * Check whether the emailId already exists in database. If exists,
         * then send a message, saying "emailId already exists"
         */
        if (StringUtils.isNotEmpty(emailId == null ? emailId : emailId.trim())) {
            Hirer userObj = hirerDao.findByEmail(emailId);
            if (null == userObj) {
                LOG.info("EmailId doesnt exists...!!!");
                hirerDao.save(hirer);
                map.put("responseCode", ResponseEnum.EMAIL_SENT.value());
                map.put("huid", hirer.getHuid());
            } else {
                String status = userObj.getStatus();
                if (status.equalsIgnoreCase("Not Authorized")) {
                    map.put("responseCode", ResponseEnum.NOT_AUTHORIZED.value());
                } else if (status.equals("Authorized")) {
                    map.put("responseCode", ResponseEnum.AUTHORIZED.value());
                } else if (status.equalsIgnoreCase("Hirer")) {
                    map.put("responseCode", ResponseEnum.SKICON.value());
                }

            }
        }
        return map;

    }

    @Override
    public Integer hirerVerification(String huid) {

        int responseCode;
        int status = hirerDao.updateStatusByUuid(huid, "Authorized", "Hirer", currentDateInStringFormat());
        if (status == 1) {
            responseCode = ResponseEnum.SUCCESS.value();
            LOG.info("Record updated!!!");
        } else {
            responseCode = ResponseEnum.FAILURE.value();
            LOG.info("Unable to update the record !!!");
        }
        return responseCode;

    }

    @Override
    public Integer saveUserDetails(Map<String, Object> hirerDetailsMap) {

        HirerAddress hirerAddress = (HirerAddress) hirerDetailsMap.get("hirerAddress");
        Hirer hirerDetails = (Hirer) hirerDetailsMap.get("hirer");
        String huid = (String) hirerDetailsMap.get("huid");
        String name = (String) hirerDetailsMap.get("name");

        Hirer hirer = hirerDao.findByHuid(huid);
            hirer.setFirstName(hirerDetails.getFirstName());
        hirer.setLastName(hirerDetails.getLastName());
        hirer.setClientType(hirerDetails.getClientType());
        hirer.setMobileNumber(hirerDetails.getMobileNumber());
        hirer.setPosition(hirerDetails.getPosition());
        hirer.setIsHirerVerified(hirerDetails.getIsHirerVerified());

        List<Integer> respList = new ArrayList<>();
        respList.add(updateHirer(hirer));
        respList.add(saveHirerAddress(hirerAddress, hirer));
        if(hirerDetailsMap.containsKey("company")){
            Company company = (Company) hirerDetailsMap.get("company");
            respList.add(saveHirerCompanyDetails(company, hirer));
        }
        for (int resp : respList) {
            if (resp == 0) {
                return ResponseEnum.FAILURE.value();
            }
        }

        int status = hirerDao.updateHirerNameAndStatusByHuid(huid, name, STATUS_HIRER,
                currentDateInStringFormat());
        return ResponseEnum.SUCCESS.value();
    }

    private Integer updateHirer(Hirer hirer) {

        Hirer hirerObj = hirerDao.save(hirer);

        if (!ObjectUtils.isEmpty(hirerObj)) {
            return 1;
        } else {
            return 0;
        }
    }


    /*********************************
     * private methds
     *********************************/

    private Integer saveHirerAddress(HirerAddress hirerAddress, Hirer hirer) {
        hirerAddress.setHirer(hirer);
        HirerAddress hirerAddressObject = hirerAddressDao.save(hirerAddress);
        if (!ObjectUtils.isEmpty(hirerAddressObject)) {
            return 1;
        } else {
            return 0;
        }
    }

    private Integer saveHirerCompanyDetails(Company company, Hirer hirer) {
        Set<Hirer> hirers = new HashSet<Hirer>();
        hirers.add(hirer);
        company.setHirer(hirers);
        Company companyObject = companyDao.save(company);
        if (!ObjectUtils.isEmpty(companyObject)) {
            return 1;
        } else {
            return 0;
        }
    }


	@Override
	public String findUser(Hirer hirer) {
		LOG.info("Login Service...!!!");
		String emailId = hirer.getEmail();
		if(StringUtils.isNotEmpty(emailId == null ? emailId : emailId.trim())){
			Hirer hirerObj = hirerDao.findHirerByEmailAndPassword(emailId,Utility.base64Encode(hirer.getPassword()));
			if(null == hirerObj){
				LOG.info("Invalid Username / Password");
				return ResponseEnum.FAILURE.value() + "##" + ""; 
			}else{
				String huid = hirerObj.getHuid();
				String status = hirerObj.getStatus();
				if(status.equals("Skicon")){
					return ResponseEnum.SKICON.value() + "##" + huid;
				}else if(status.equals("Authorized")){
					return ResponseEnum.AUTHORIZED.value() + "##" + huid;
				}else if(status.equals("Not Authorized")){
					return ResponseEnum.NOT_AUTHORIZED.value() + "##" + huid;
				}
			}
		}
		return null;
	}

    private String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }

}
