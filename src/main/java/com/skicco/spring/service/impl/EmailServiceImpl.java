package com.skicco.spring.service.impl;

import com.skicco.spring.dto.Mail;
import com.skicco.spring.service.EmailService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
@Service
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	private JavaMailSender sender;
	
	@Autowired
	private Configuration freeMarkerConfig;

	@Override
	public String sendEmailToVerifyUser(String emailId,String uuid) {
		Map<String, Object> map = new HashMap<>();
		Mail mail = new Mail();
		mail.setMailTo(emailId);
		mail.setMailSubject("Congratulations.!! Welcome to skicco.com");// get it from the enums
		map.put("uuid", uuid);
		mail.setModel(map);
		String template = "email-verification.ftl";
		return sendMail(mail,template);
	}
	
	@Override
	public String sendEmailToVerifyHirer(String emailId,String huid) {
		Map<String, Object> map = new HashMap<>();
		Mail mail = new Mail();
		mail.setMailTo(emailId);
		mail.setMailSubject("Welcome to skicco.com..!!");// get it from the enums
		map.put("huid", huid);
		mail.setModel(map);
		String template = "hirer-email-verification.ftl";
		return sendMail(mail,template);
	}

	private String sendMail(Mail mail,String template){
		String status="success";
		
		MimeMessage msg = sender.createMimeMessage();
		MimeMessageHelper msgHelper = new MimeMessageHelper(msg);
		
		try {
		
		freeMarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");
		
		Template templateVal = freeMarkerConfig.getTemplate(template);
		String text;
		
		text = FreeMarkerTemplateUtils.processTemplateIntoString(templateVal, mail.getModel());
		
		msgHelper.setTo(mail.getMailTo());
		msgHelper.setText(text,true);
		msgHelper.setSubject(mail.getMailSubject());
		
		sender.send(msg);
		
		} catch (IOException | TemplateException | MessagingException e) {
			status="failure";
			e.printStackTrace();
		}
		return status;
	}
	
	

}
