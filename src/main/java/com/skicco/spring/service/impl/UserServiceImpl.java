package com.skicco.spring.service.impl;

import com.skicco.spring.dao.*;
import com.skicco.spring.entity.*;
import com.skicco.spring.service.UserService;
import com.skicco.spring.util.ResponseEnum;
import com.skicco.spring.util.Utility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.*;

/**
 * @author KRISHNA PANDU RANGA
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);

    //************************************************************************
    // Constants
    //************************************************************************
    private static final String RESPONSE_CODE = "responseCode";
    private static final String AUTHORIZED = "Authorized";
    private static final String NOT_AUTHORIZED = "Not Authorized";
    private static final String SKILLS_REQUIRED = "Skills Required";
    private static final String SKICON = "Skicon";
    private static final String USER = "USER";

    //************************************************************************
    // Class Attributes
    //************************************************************************

    @Autowired
    UserDao userDao;

    @Autowired
    UserDetailsDao userDetailsDao;

    @Autowired
    UserSkillDao userSkillDao;

    @Autowired
    UserAddressDao userAddressDao;

    @Autowired
    UserEducationDao userEducationDao;

    @Autowired
    UserPreferencesDao userPreferencesDao;

    //************************************************************************
    // Overridden method(s)
    //************************************************************************

    /**
     * This method is used for SignUp of an User
     * If the User is already present in the database, then the ResponseCode is returned with the status.
     * If the User hasn't signedUp yet, then the User Info is saved in the database.
     *
     * @param user
     * @return Map with ResponseCode and UUID
     */
    @Override
    public Map<String, Object> signUp(User user) {
        Map<String, Object> map = new HashMap<>();
        LOG.info("SignUp Service...!!!");
        String emailId = user.getEmailId();

        if (StringUtils.isNotEmpty(emailId == null ? emailId : emailId.trim())) {
            User userObj = userDao.findByEmailId(emailId);
            if (null == userObj) {
                LOG.info("EmailId doesn't exists...!!!");
                userDao.save(user);
                map.put(RESPONSE_CODE, ResponseEnum.EMAIL_SENT.value());
                map.put("uuid", user.getUuid());
            } else {
                String status = userObj.getStatus();
                if (status.equalsIgnoreCase(NOT_AUTHORIZED)) {
                    map.put(RESPONSE_CODE, ResponseEnum.NOT_AUTHORIZED.value());
                } else if (status.equals(AUTHORIZED)) {
                    map.put(RESPONSE_CODE, ResponseEnum.AUTHORIZED.value());
                } else if (status.equalsIgnoreCase(SKICON)) {
                    map.put(RESPONSE_CODE, ResponseEnum.SKICON.value());
                } else if (status.equalsIgnoreCase(SKILLS_REQUIRED)) {
                    map.put(RESPONSE_CODE, ResponseEnum.SKICON.value());
                }
            }
        }
        return map;
    }

    @Override
    public Integer userVerification(String uuid) {
        int responseCode = 0;
        User user = userDao.findByUuid(uuid);
        String status = user.getStatus();
        if (status.equalsIgnoreCase(NOT_AUTHORIZED)) {
            int statusObj = userDao.updateStatusByUuid(uuid, AUTHORIZED, USER, currentDateInStringFormat());
            LOG.info("status : {}", status);
            if (statusObj == 1) {
                responseCode = ResponseEnum.SUCCESS.value();
            } else {
                responseCode = ResponseEnum.FAILURE.value();
            }
        } else if (status.equalsIgnoreCase(AUTHORIZED) || status.equalsIgnoreCase(SKILLS_REQUIRED) || status.equalsIgnoreCase(SKICON)) {
            responseCode = ResponseEnum.AUTHORIZED.value();
            LOG.info("already authorised {}", responseCode);
        }
        LOG.info("final response code {}", responseCode);
        return responseCode;

    }

    @Override
    public Map<String, Object> findUser(final User user) {
        LOG.info("Login Service...!!!");
        Map<String, Object> map = new HashMap<>();
        String emailId = user.getEmailId();

        if (StringUtils.isNotEmpty(emailId == null ? emailId : emailId.trim())) {
            User userObj = userDao.findUserByEmailIdAndPwd(emailId, user.getPwd());
            if (null == userObj) {
                LOG.info("Invalid Username / Password");
                map.put("response", ResponseEnum.FAILURE.value());
            } else {
                map.put("user", userObj);
                String uuid = userObj.getUuid();
                LOG.info("Username found : {} ,UUID : {}", userObj.getEmailId(), uuid);
                String status = userObj.getStatus();
                if (status.equals(SKICON)) {
                    map.put("response", ResponseEnum.SKICON.value());
                } else if (status.equals(AUTHORIZED)) {
                    map.put("response", ResponseEnum.AUTHORIZED.value());
                } else if (status.equals(NOT_AUTHORIZED)) {
                    map.put("response", ResponseEnum.NOT_AUTHORIZED.value());
                } else if (status.equals(SKILLS_REQUIRED)) {
                    map.put("response", ResponseEnum.SKILLS_REQUIRED.value());
                }
            }
        }
        return map;
    }

    @Override
    public Integer saveUserDetails(final Map<String, Object> userDetailsMap) {
        // Update User Table with UserName and Status
        UserAddress userAddress = (UserAddress) userDetailsMap.get("userAddress");
        UserDetails userDetails = (UserDetails) userDetailsMap.get("userDetails");
        UserEducation userEducation = (UserEducation) userDetailsMap.get("userEducation");
        UserPreferences userPreferences = (UserPreferences) userDetailsMap.get("userPreferences");
        String uuid = (String) userDetailsMap.get("uuid");
        String name = (String) userDetailsMap.get("name");

        User user = userDao.findByUuid(uuid);
        List<Integer> respList = new ArrayList<>();
        respList.add(saveUserAddress(userAddress, user));
        respList.add(saveUserDetails(userDetails, user));
        respList.add(saveUserEducation(userEducation, user));
        respList.add(saveUserPreferences(userPreferences, user));
        for (int resp : respList) {
            if (resp == 0) {
                return ResponseEnum.FAILURE.value();
            }
        }
        int status = userDao.updateUserNameAndStatusByUUID(uuid, name, SKILLS_REQUIRED,
                currentDateInStringFormat());
        if (status == 1) {
            LOG.info("User : {} Record Updated !!!", uuid);
            return ResponseEnum.SUCCESS.value();
        } else {
            LOG.info("User Record not Updated");
            return ResponseEnum.FAILURE.value();
        }
    }

    @Override
    public Map<String, Object> saveUserSkillDetails(final UserSkill userSkill, final String uuid) {
        LOG.info("saveUserSkillDetails Service...!!!");
        Map<String, Object> map = new HashMap<>();

        try {
            User user = userDao.findByUuid(uuid);
            String name = user.getName();
            int status = userDao.updateUserNameAndStatusByUUID(uuid, name, SKICON,
                    currentDateInStringFormat());
            if (status == 1) {
                LOG.info("User : Record {} Updated as Skiccon !!!", uuid);
                //Convert Map to Json String
                userSkill.setUser(user);
                userSkillDao.save(userSkill);
                map.put("response", ResponseEnum.SUCCESS.value());
                map.put("user", user);
            } else {
                LOG.info("User Record not Updated as skicon");
                map.put("response", ResponseEnum.FAILURE.value());
            }
            return map;
        } catch (Exception exception) {
            LOG.error(exception.getMessage());
            map.put("response", ResponseEnum.FAILURE.value());
            return map;
        }
    }

    @Override
    public User getUser(final String uuid) {
        return userDao.findByUuid(uuid);
    }
    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private Integer saveUserAddress(UserAddress userAddress, User user) {
        userAddress.setUser(user);
        UserAddress userAddressObject = userAddressDao.save(userAddress);
        if (!ObjectUtils.isEmpty(userAddressObject)) {
            return 1;
        } else {
            return 0;
        }
    }

    private Integer saveUserDetails(UserDetails userDetails, User user) {
        userDetails.setUser(user);
        UserDetails isUserDetailsSaved = userDetailsDao.save(userDetails);
        if (isUserDetailsSaved != null) {
            return 1;
        } else {
            return 0;
        }
    }

    private Integer saveUserEducation(UserEducation userEducation, User user) {
        userEducation.setUser(user);
        UserEducation isUserEducationSaved = userEducationDao.save(userEducation);
        if (isUserEducationSaved != null) {
            return 1;
        } else {
            return 0;
        }
    }

    private Integer saveUserPreferences(UserPreferences userPreferences, User user) {
        userPreferences.setUser(user);
        UserPreferences isUserPreferencesSaved = userPreferencesDao.save(userPreferences);
        if (isUserPreferencesSaved != null) {
            return 1;
        } else {
            return 0;
        }
    }

    private String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }

}
