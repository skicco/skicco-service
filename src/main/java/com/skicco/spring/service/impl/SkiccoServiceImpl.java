package com.skicco.spring.service.impl;

import com.skicco.spring.dao.SkillDetailsDao;
import com.skicco.spring.entity.SkillDetails;
import com.skicco.spring.service.SkiccoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 */
@Service
public class SkiccoServiceImpl implements SkiccoService {

    private static final Logger LOG = LoggerFactory.getLogger(SkiccoServiceImpl.class);

    @Autowired
    SkillDetailsDao skillDetailsDao;
    @Override
    public List<SkillDetails> retrieveSkillDetails() {
        LOG.info("RetrieveSkillDetails....");
        List<SkillDetails> skillDetailsList = skillDetailsDao.findAll();
        sortSkillDetailsInAscOrder(skillDetailsList);
        return skillDetailsList;
    }

    private void sortSkillDetailsInAscOrder(List<SkillDetails> skillDetailsList) {

        skillDetailsList.forEach((skillDetails -> skillDetails.setSubSkills(Arrays.asList(skillDetails.getSubskillsStr().split(",")))));

        //Sort skills in Ascending Order
        skillDetailsList.sort((SkillDetails s1, SkillDetails s2) -> s1.getSkill().compareTo(s2.getSkill()));

        //Pass the sorted list of skills, to sort the List of sub skills by iterating skills
        skillDetailsList.forEach((skillDetails -> {
            skillDetails.getSubSkills().sort((String s1, String s2) -> s1.compareToIgnoreCase(s2));
            skillDetails.setDisplayKey(skillDetailsList.indexOf(skillDetails));
        } ));

    }

}
