package com.skicco.spring.service.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skicco.spring.dao.HirerDao;
import com.skicco.spring.dao.ProjectDao;
import com.skicco.spring.dao.UserSkillDao;
import com.skicco.spring.entity.Project;
import com.skicco.spring.entity.UserSkill;
import com.skicco.spring.model.SubSkill;
import com.skicco.spring.service.ProjectService;
import com.skicco.spring.util.ResponseEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

/**
 * @author KRISHNA PANDU RANGA
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectServiceImpl.class);

    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private HirerDao hirerDao;
    @Autowired
    private UserSkillDao userSkillDao;


   /* @Autowired
    private ShowInterestDao showInterestDao;*/

    //************************************************************************
    // Overridden method(s)
    //************************************************************************

    @Override
    public Integer postProjectDetails(Project project) {
        project.setHirer(hirerDao.findOne(project.getHirer().getHuid()));
        Project projectObj = projectDao.save(project);
        if (null != projectObj) {
            LOG.info("Project saved successfully..!!" + projectObj.getHirer().getHuid());
            return ResponseEnum.SUCCESS.value();
        } else {
            return ResponseEnum.FAILURE.value();
        }
    }

    /**
     * Retrieve the Projects based on the criteria that
     * the Status of the Project should be either Initial (100), Not Started (101), Started (102), In Progress (103)
     * and No.of.Vacancies is not equal to 0 in a project
     * <p>
     * After retrieving of the projects from the project table, we need to get the User Skill details based on uuid
     * <p>
     * After getting the User Skills, first we iterate the Projects, get the skill. Based on the skill, get the User Sub Skills
     * if any of the Sub skill of the Project contains in User Sub skills, then the project is considered as userSkillProject
     * and the userSkillProject is removed from the projects list.
     * <p>
     * So a Map is formed with UserSkillProject and OtherProject and is returned to controller.
     *
     * @param uuid
     * @param index
     * @return Map<String, List < PostProject>>
     */
    @Override
    public List<Project> retrieveSkiconSkilledProjects(final String uuid, final int index) {
        final List<Integer> statuses = Arrays.asList(100, 101, 102, 103);
        final String subSkills = buildUserSubSkills(uuid);
        return new CopyOnWriteArrayList<>(projectDao.findSkiconProjectsByStatusAndNoOfVacancies(statuses, subSkills, index * 5));
    }

    @Override
    public List<Project> retrieveSkiconOtherSkilledProjects(final String uuid, final int index) {
        final List<Integer> statuses = Arrays.asList(100, 101, 102, 103);
        final String subSkills = buildUserSubSkills(uuid);
        return new CopyOnWriteArrayList<>(projectDao.findSkiconOtherSkilledProjectsByStatusAndNoOfVacancies(statuses, subSkills, index * 5));
    }

   /* @Override
    public Map<String, List<Project>> retrieveHirerRelatedProjects(String huid) {
        return null;
    }

    */

    /**
     * This method inserted the Shown Interested Project to show_interest table
     *
     * @param
     * @return 200 if the shownInterestProject is saved successfully. Else returns 0.
     *//*
    @Override
    public Integer saveShownInterestedProject(ShowInterest showInterest) {
        showInterest.setProjectIdStr("PRJ" + String.format("%06d", showInterest.getProjectId()));
        showInterest.setStatus(ProjectStatusEnum.INITIAL.value());
        GenericDetails genericDetails = new GenericDetails();
        genericDetails.setCreatedBy(showInterest.getUserName());
        genericDetails.setCreatedTime(currentDateInStringFormat());

        showInterest.setGenericDetails(genericDetails);
        ShowInterest showInterestObj = showInterestDao.save(showInterest);
        if (null != showInterestObj) {
            LOG.info("Shown Interested Project saved successfully..!!" + showInterestObj.getProjectIdStr());
            return ResponseEnum.SUCCESS.value();
        } else {
            return ResponseEnum.FAILURE.value();
        }
    }
*/
    //************************************************************************
    // Private method(s)
    //************************************************************************
    private String buildUserSubSkills(final String uuid) {
        ObjectMapper objectMapper = new ObjectMapper();
        //Get the User Skill Details
        Map<String, List<SubSkill>> userSkillsMap = null;
        final UserSkill userSkillDetails = userSkillDao.findByUuid(uuid);
        String subSkills = "";
        try {
            userSkillsMap = objectMapper.readValue(userSkillDetails.getSkillDetails(),
                    new TypeReference<Map<String, List<SubSkill>>>() {
                    });
            //Combining all the lists into one
            List<SubSkill> userSubSkillsList = userSkillsMap.values()
                    .stream().flatMap(Collection::stream).collect(Collectors.toList());
            //generate set of subSkills from the List of SubSkills object
            Set<String> subSkillsSet = userSubSkillsList.stream()
                    .map(SubSkill::getSubSkill)
                    .collect(Collectors.toSet());
            subSkills = String.join("|", subSkillsSet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return subSkills;
    }
}
