package com.skicco.spring.service;

import com.skicco.spring.entity.SkillDetails;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
public interface SkiccoService {

	List<SkillDetails> retrieveSkillDetails();
}
