package com.skicco.spring.service;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
public interface EmailService {

	String sendEmailToVerifyUser(String emailId, String uuid);

	String sendEmailToVerifyHirer(String emailId, String huid);

}
