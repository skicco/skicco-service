package com.skicco.spring.service;

import com.skicco.spring.entity.User;
import com.skicco.spring.entity.UserSkill;

import java.util.Map;

/**
 * @author KRISHNA PANDU RANGA
 */
public interface UserService {

    User getUser(final String uuid);

    Map<String, Object> signUp(final User user);

    Integer userVerification(final String uuid);

    Map<String, Object> findUser(final User user);

    Integer saveUserDetails(final Map<String, Object> userDetailsMap);

    Map<String, Object> saveUserSkillDetails(final UserSkill userSkill, final String uuid);
}
