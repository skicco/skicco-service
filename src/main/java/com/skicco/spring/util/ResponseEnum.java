package com.skicco.spring.util;

/**
 * ResponseEnum
 * @author KRISHNA PANDU RANGA
 * This Enum will be used for intimate status on respective occasions.
 * SUCCESS - 200 - will be used in success.
 * NOT_AUTHORIZED - 202 - will be used when user signed up but email address need to be verified.
 * AUTHORIZED - 203 - will be used when user email is verified but user details needed.
 * SKILLS_REQUIRED - 204 - will be used when user details updated and skills needed.
 * SKICON - 205 - will be used when user is skicon.
 * FAILURE - 0 - this status will be used when there is a failure.
 */
public enum ResponseEnum {
	
	SUCCESS(200), EMAIL_SENT (201), NOT_AUTHORIZED (202), AUTHORIZED (203) ,SKILLS_REQUIRED(204), SKICON(205), FAILURE (0);
	
	private int responseCode;
	
	private ResponseEnum(int responseCode){
		this.responseCode = responseCode;
	}
	
	public int value(){
		return responseCode;
	}

}
