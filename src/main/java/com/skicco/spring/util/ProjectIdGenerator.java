package com.skicco.spring.util;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.id.IdentifierGenerator;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ProjectIdGenerator implements IdentifierGenerator {
    private static final String PREFIX = "PRJ";

    @Override
    public Serializable generate(SessionImplementor session, Object object)
            throws HibernateException {

        Connection connection = session.connection();

        try {
            Statement statement = connection.createStatement();

            ResultSet rs = statement.executeQuery("select count(id) as Id from project");

            if (rs.next()) {
                int id = rs.getInt(1) + 1001;
                String generatedId = PREFIX + new Integer(id).toString();
                return generatedId;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return null;
    }


}
