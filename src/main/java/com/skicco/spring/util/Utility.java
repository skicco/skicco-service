package com.skicco.spring.util;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

//import org.springframework.security.crypto.codec.Base64;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
public class Utility {
	
	static SimpleDateFormat dateformatJava = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static String base64Encode(String code){
		byte[] encodedBytes = Base64.getEncoder().encode(code.getBytes());
		return new String(encodedBytes,Charset.forName("UTF-8"));
	}
	
	public static String base64Decode(String code){
		byte[] decodedBytes = Base64.getDecoder().decode(code.getBytes());
		return new String(decodedBytes,Charset.forName("UTF-8"));
	}
	
	public static String convertDateToString(Date date){
		return dateformatJava.format(date);
	}
}
