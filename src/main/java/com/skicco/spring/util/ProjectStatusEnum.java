package com.skicco.spring.util;

/**
 *
 * This corresponding Enum holds the status of the Projects
 * Without Payment - Initial
 * After Payment - Not Started
 * After User Allotted for Project - Started
 * User Started working - In Progress
 * User Delivered the Project - Completed
 * User left from the Project - User abandoned
 * Hirer left from the Project - Discarded
 * Initiating Payment for Project - Payment Initiation
 * Payment pending for Project - Payment Pending
 *
 * @author KRISHNA PANDU RANGA
 */
public enum ProjectStatusEnum {

    INITIAL(100), NOT_STARTED(101), STARTED(102), IN_PROGRESS(103), COMPLETED(104), USER_ABANDONDED(105), DISCARDED(106), PAYMENT_INITIATION(107), PAYMENT_PENDING(108);

    private int responseCode;

    private ProjectStatusEnum(int responseCode) {
        this.responseCode = responseCode;
    }

    public int value() {
        return responseCode;
    }

}
