package com.skicco.spring.util;

public class SkiccoConstants {

    public static final String FIELD_HUID = "huid";
    public static final String SECRET_KEY = "c2tpY2Nv";
    public static final String KEY_AUTHORIZATION = "Authorization";
    public static final String VALUE_HIRER = "Hirer";
    public static final String VALUE_SKICON = "Skicon";
    public static final String VALUE_SKICCO = "SKICCO";
    public static final String VALUE_UUID = "uuid";

}
