package com.skicco.spring.util.jwt;


import com.skicco.spring.util.SkiccoConstants;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;

import java.util.Base64;

/**
 * This class will be used to verify the self signed token for secured transactions
 * through all microservices
 *
 * @Author Sri Harsha
 * @Date 04/01/2020
 */
public class JwtTokenChecker {

    //****************************************************************
    // Class Attributes
    //***************************************************************
    private static final String secretKey = Base64.getEncoder().encodeToString(SkiccoConstants.SECRET_KEY.getBytes());

    //****************************************************************
    // Public Method(s)
    //****************************************************************

    /**
     * this method will consume the jwt and return respective uid after extraction
     * @param token
     * @param user  should be either huid or uuid
     * @return uid
     */
    public static String authenticate(final String token, final String user) {
        final Jws<Claims> jwsClaims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token);
        return (String) jwsClaims.getBody().get(user);
    }
}
