package com.skicco.spring.facade;

import com.skicco.spring.dto.HirerDTO;
import com.skicco.spring.dto.HirerDetailsDTO;

/**
 * @author SriHarsha
 *
 */
public interface HirerFacade {
	
	Integer signUp(HirerDTO hirer);
	
	Integer hirerVerification(String huid);

	Integer saveHirerDetails(HirerDetailsDTO hirerDetailsDTO);

	String login(HirerDTO hirer);

}
