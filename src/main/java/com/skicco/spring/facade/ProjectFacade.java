package com.skicco.spring.facade;

import com.skicco.spring.dto.ProjectDTO;

import java.util.List;

/**
 * @author SriHarsha
 */
public interface ProjectFacade {

    Integer postProjectDetails(final ProjectDTO project);

    List<ProjectDTO> retrieveSkiconSkilledProjects(final String uuid, final int index);

    List<ProjectDTO> retrieveSkiconOtherSkilledProjects(final String uuid, final int index);
     /*Integer saveShownInterestedProject(ShowInterest showInterest);

    Map<String, List<Project>> retrieveHirerRelatedProjects(String huid);*/
}
