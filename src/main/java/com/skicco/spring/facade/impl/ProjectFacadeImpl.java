package com.skicco.spring.facade.impl;

import com.skicco.spring.converter.ProjectConverter;
import com.skicco.spring.dto.ProjectDTO;
import com.skicco.spring.entity.Project;
import com.skicco.spring.facade.ProjectFacade;
import com.skicco.spring.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author SriHarsha
 */
@Component
public class ProjectFacadeImpl implements ProjectFacade {

    @Autowired
    ProjectService projectService;

    public Integer postProjectDetails(final ProjectDTO projectDTO) {
        Project project = ProjectConverter.convertProjectDtoToProjectEntity(projectDTO);
        return projectService.postProjectDetails(project);
    }

    @Override
    public List<ProjectDTO> retrieveSkiconSkilledProjects(final String uuid, final int index) {
        final List<Project> projects = projectService.retrieveSkiconSkilledProjects(uuid, index);
        return ProjectConverter.convertProjectEntityToProjectDto(projects);
    }

    @Override
    public List<ProjectDTO> retrieveSkiconOtherSkilledProjects(final String uuid, final int index) {
        final List<Project> projects = projectService.retrieveSkiconOtherSkilledProjects(uuid, index);
        return ProjectConverter.convertProjectEntityToProjectDto(projects);
    }

	/*@Override
	public Integer saveShownInterestedProject(ShowInterest showInterest) {
		return projectsService.saveShownInterestedProject(showInterest);
	}

	@Override
	public Map<String, List<Project>> retrieveHirerRelatedProjects(String huid) {
		return projectsService.retrieveHirerRelatedProjects(huid);
	}*/
}
