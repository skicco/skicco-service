/**
 * 
 */
package com.skicco.spring.facade.impl;

import com.skicco.spring.converter.HirerConverter;
import com.skicco.spring.converter.HirerDetailsConverter;
import com.skicco.spring.dto.HirerDTO;
import com.skicco.spring.dto.HirerDetailsDTO;
import com.skicco.spring.entity.Hirer;
import com.skicco.spring.facade.HirerFacade;
import com.skicco.spring.service.EmailService;
import com.skicco.spring.service.HirerService;
import com.skicco.spring.util.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author SriHarsha
 *
 */
@Component
public class HirerFacadeImpl implements HirerFacade{
	
	@Autowired
	HirerService hirerService;
	
	@Autowired
	EmailService emailService;

	/**
	 *
	 * @param hirerDTO
	 * @return responseCode
	 */
	@Override
	public Integer signUp(HirerDTO hirerDTO) {
		Hirer hirer = HirerConverter.convertHirerDtoToHirerEntity(hirerDTO);
		Map<String, Object> map = hirerService.signUp(hirer);
		if(!map.isEmpty()){
			int responseCode = (Integer)map.get("responseCode");
			if(responseCode == ResponseEnum.EMAIL_SENT.value()){
				emailService.sendEmailToVerifyHirer(hirerDTO.getEmailId(),String.valueOf(map.get("huid")));
			}
			return responseCode;
		}
		return null;
	}

	@Override
	public Integer hirerVerification(String huid) {
		return hirerService.hirerVerification(huid);
	}

	@Override
	public Integer saveHirerDetails(HirerDetailsDTO hirerDetailsDTO) {
		final Map<String, Object> hirerDetailsMap = HirerDetailsConverter.convertUserDetailsDtoToEntity(hirerDetailsDTO);
		return hirerService.saveUserDetails(hirerDetailsMap);
	}

	@Override
	public String login(HirerDTO hirerDTO) {
		Hirer hirer = HirerConverter.convertHirerDtoToHirerForLogin(hirerDTO);
		return hirerService.findUser(hirer);
	}


}
