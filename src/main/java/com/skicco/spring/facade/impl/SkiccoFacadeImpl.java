package com.skicco.spring.facade.impl;

import com.skicco.spring.entity.SkillDetails;
import com.skicco.spring.facade.SkiccoFacade;
import com.skicco.spring.service.SkiccoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
@Component
public class SkiccoFacadeImpl implements SkiccoFacade{

	@Autowired
	SkiccoService skiccoService;
	
	@Override
	public List<SkillDetails> retrieveSkillDetails() {
		return skiccoService.retrieveSkillDetails();
	}

}
