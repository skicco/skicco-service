package com.skicco.spring.facade.impl;

import com.skicco.spring.converter.UserConverter;
import com.skicco.spring.converter.UserDetailsConverter;
import com.skicco.spring.converter.UserSkillConverter;
import com.skicco.spring.dto.UserDTO;
import com.skicco.spring.dto.UserDashBoardDetailsDTO;
import com.skicco.spring.dto.UserDetailsRequestDTO;
import com.skicco.spring.dto.UserSkillDTO;
import com.skicco.spring.entity.User;
import com.skicco.spring.entity.UserSkill;
import com.skicco.spring.facade.UserFacade;
import com.skicco.spring.service.EmailService;
import com.skicco.spring.service.UserService;
import com.skicco.spring.util.ResponseEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Map;

/**
 * @author KRISHNA PANDU RANGA
 */
@Component
public class UserFacadeImpl implements UserFacade {

    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    //************************************************************************
    // Overridden method(s)
    //************************************************************************

    @Override
    public Integer signUp(final UserDTO userDTO) {
        User user = UserConverter.convertUserDtoToEntity(userDTO);
        Map<String, Object> map = userService.signUp(user);
        if (!map.isEmpty()) {
            int responseCode = (Integer) map.get("responseCode");
            if (responseCode == ResponseEnum.EMAIL_SENT.value()) {
                emailService.sendEmailToVerifyUser(user.getEmailId(), String.valueOf(map.get("uuid")));
            }
            return responseCode;
        }
        return null;
    }

    @Override
    public Integer userVerification(final String uuid) {
        return userService.userVerification(uuid);
    }

    @Override
    public Map<String, Object> login(final UserDTO userDTO) {
        final User user = UserConverter.convertUserDtoToEntity(userDTO);
        final Map<String, Object> map = userService.findUser(user);
        final User userEntity = (User) map.get("user");
        if(!ObjectUtils.isEmpty(userEntity)) {
            UserDashBoardDetailsDTO userDashBoardDetailsDTO = UserDetailsConverter.convertUserEntityToUserDashBoardSetailsDTO(userEntity);
            map.put("user", userDashBoardDetailsDTO);
        }
        return map;
    }

    @Override
    public Integer saveUserDetails(final UserDetailsRequestDTO userDetailsRequestDTO) {
        final Map<String, Object> userDetailsMap = UserDetailsConverter.convertUserDetailsDtoToEntity(userDetailsRequestDTO);
        return userService.saveUserDetails(userDetailsMap);
    }

    @Override
    public  Map<String, Object> saveUserSkillDetails(final UserSkillDTO userSkillDTO) {
        final UserSkill userSkill = UserSkillConverter.convertUserSkillDtoToUserSkillEntity(userSkillDTO);
        final Map<String, Object> map =  userService.saveUserSkillDetails(userSkill, userSkillDTO.getUuid());
        final User userEntity = (User) map.get("user");
        UserDashBoardDetailsDTO userDashBoardDetailsDTO = UserDetailsConverter.convertUserEntityToUserDashBoardSetailsDTO(userEntity);
        map.put("user", userDashBoardDetailsDTO);
        return map;

    }

    @Override
    public UserDashBoardDetailsDTO getUser(String uuid) {
        User user = userService.getUser(uuid);
        final UserDashBoardDetailsDTO userDashBoardDetailsDTO = UserDetailsConverter.convertUserEntityToUserDashBoardSetailsDTO(user);
        return userDashBoardDetailsDTO;
    }

}