package com.skicco.spring.facade;

import com.skicco.spring.dto.UserDTO;
import com.skicco.spring.dto.UserDashBoardDetailsDTO;
import com.skicco.spring.dto.UserDetailsRequestDTO;
import com.skicco.spring.dto.UserSkillDTO;

import java.util.Map;

/**
 * @author KRISHNA PANDU RANGA
 */
public interface UserFacade {

    Integer signUp(final UserDTO userDTO);

    Integer userVerification(final String uuid);

    Map<String, Object> login(final UserDTO userDTO);

    Integer saveUserDetails(final UserDetailsRequestDTO userDetailsRequestDTO);

    Map<String, Object> saveUserSkillDetails(final UserSkillDTO userSkillDTO);

    UserDashBoardDetailsDTO getUser(String Uuid);

}
