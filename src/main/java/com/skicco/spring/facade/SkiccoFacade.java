package com.skicco.spring.facade;

import com.skicco.spring.entity.SkillDetails;

import java.util.List;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
public interface SkiccoFacade {

	List<SkillDetails> retrieveSkillDetails();
}
