package com.skicco.spring.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author KRISHNA PANDU RANGA
 *
 */
@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages="com.skicco.spring.entity")
@ComponentScan(basePackages="com.skicco.spring")
@EnableJpaRepositories(basePackages="com.skicco.spring.dao")
public class SkiccoApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SkiccoApplication.class, args);
	}
	
}
