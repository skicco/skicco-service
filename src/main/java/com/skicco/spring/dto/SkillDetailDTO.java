package com.skicco.spring.dto;

import lombok.Data;

@Data
public class SkillDetailDTO {

    private int id;
    private String skill;
    private String subSkills;
    private int displayKey;
}
