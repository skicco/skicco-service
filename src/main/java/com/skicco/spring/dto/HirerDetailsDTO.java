package com.skicco.spring.dto;

import lombok.Data;

/**
 * @author SriHarsha
 */
@Data
public class  HirerDetailsDTO {

    private String huid;
    private String firstName;
    private String lastName;
    private String clientType;
    private String companyName;
    private String position;
    private String mobileNumber;
    private Boolean isHirerVerified;
    private HirerAddressDTO hirerAddressDTO;

}