package com.skicco.spring.dto;

import lombok.Data;

@Data
public class SubSkillDTO {
    private String subSkill;
    private float rating;

    //*********************************************
    //Constructor
    //********************************************

    public SubSkillDTO() {
    }

    public SubSkillDTO(String subSkill, float rating) {
        this.subSkill = subSkill;
        this.rating = rating;
    }
}
