package com.skicco.spring.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserPreferancesDTO {

    private String fromTime;
    private String toTime;
    private String distanceTravel;
    private int noOfHours;
}
