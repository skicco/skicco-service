package com.skicco.spring.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsDTO {
    private int id;
    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String aadharNumber;
    private String introduction;
}
