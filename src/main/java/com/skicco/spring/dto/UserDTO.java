package com.skicco.spring.dto;

import lombok.Data;

@Data
public class UserDTO {

    private String id;
    private String emailId;
    private String name;
    private String pwd;
    private String status;
}
