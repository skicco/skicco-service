package com.skicco.spring.dto;

import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.UserSkillDetails;
import lombok.Data;

import java.util.List;
@Data
public class UserSkillDTO {

    private String uuid;
    private List<UserSkillDetails> userSkills;
    private GenericDetails genericDetails;

}
