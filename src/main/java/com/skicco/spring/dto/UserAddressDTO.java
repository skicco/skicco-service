package com.skicco.spring.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserAddressDTO {

    private String doorNumber;
    private String city;
    private String state;
    private String country;
    private int zipCode;
}
