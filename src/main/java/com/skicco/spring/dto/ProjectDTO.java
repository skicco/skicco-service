package com.skicco.spring.dto;

import com.skicco.spring.entity.GenericDetails;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.GeneratedValue;
import java.util.List;

@Data
public class ProjectDTO {

    @GenericGenerator(name = "sequence_prj_id", strategy = "com.skicco.spring.util.ProjectIdGenerator")
    @GeneratedValue(generator = "sequence_prj_id")
    private String id;
    private String title;
    private String skill;
    private String huid;
    private String hirerName;
    private List<String> subSkills;
    private int numberOfResources;
    private String budgetType;
    private String estimatedBudget;
    private String estimatedTime;
    private String description;
    private String estimatedStartTime;
    private String prefCountry;
    private String cutOffDateForShowInterest;
    private String filesPath;
    private String locationType;
    private String addressLine;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private GenericDetails genericDetails;
}
