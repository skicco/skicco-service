package com.skicco.spring.dto;

import lombok.Data;

/**
 * @author SriHarsha
 */
@Data
public class HirerAddressDTO {

    private String addr;
    private String city;
    private String state;
    private String country;
    private String zipCode;
}
