package com.skicco.spring.dto;

import lombok.Data;

import java.util.Set;

@Data
public class UserDashBoardDetailsDTO {

    private String uuid;
    private String name;
    private UserDetailsDTO userDetails;
    private String userSkill;
    private Set<UserAddressDTO> userAddress;
    private Set<UserEducationDTO> userEducation;
    private UserPreferancesDTO userPreferences;


}
