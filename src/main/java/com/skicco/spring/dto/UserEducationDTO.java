package com.skicco.spring.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserEducationDTO {

    private String educationQualification;
    private String educationSpecialization;
    private int yearOfCompletion;
    private String collegeName;
}
