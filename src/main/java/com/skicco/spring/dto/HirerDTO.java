package com.skicco.spring.dto;

import lombok.Data;

/**
 * @author SriHarsha
 *
 */

@Data
public class HirerDTO {

    private String id;
    private String emailId;
    private String name;
    private String pwd;
    private String status;

}
