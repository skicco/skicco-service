package com.skicco.spring.dto;


import com.skicco.spring.entity.GenericDetails;
import lombok.Data;

@Data
public class UserDetailsRequestDTO {


    private String uuid;
    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String city;
    private String state;
    private String country;
    private int zipCode;
    private String aadharNum;
    private String educationQualification;
    private String educationSpecialization;
    private int yearOfCompletion;
    private String collegeName;
    private int noOfHours;
    private String fromTime;
    private String toTime;
    private String distanceTravel;
    private String introduction;
    private String doorNumber;
}
