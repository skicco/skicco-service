package com.skicco.spring.converter;

import com.skicco.spring.dto.ProjectDTO;
import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.Hirer;
import com.skicco.spring.entity.Project;
import com.skicco.spring.entity.ProjectAddress;
import com.skicco.spring.util.ProjectStatusEnum;
import com.skicco.spring.util.Utility;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectConverter {

    //************************************************************************
    // Public method(s)
    //************************************************************************

    public static Project convertProjectDtoToProjectEntity(ProjectDTO projectDTO) {
        Project project = new Project();
        ProjectAddress projectAddress = new ProjectAddress();
        Hirer hirer = new Hirer();

        hirer.setHuid(projectDTO.getHuid());
        project.setHirer(hirer);

        projectAddress.setAddress1(projectDTO.getAddressLine());
        projectAddress.setCity(projectDTO.getCity());
        projectAddress.setState(projectDTO.getState());
        projectAddress.setCountry(projectDTO.getCountry());
        projectAddress.setZipCode(projectDTO.getZipCode());
        projectAddress.setGenericDetails(getGenericDetails());
        Set<ProjectAddress> projectAddressSet = new HashSet();
        projectAddressSet.add(projectAddress);

        project.setNumberOfVacancies(projectDTO.getNumberOfResources());
        project.setSubSkills(projectDTO.getSubSkills().stream()
                .collect(Collectors.joining(",")));
        project.setBudgetType(projectDTO.getBudgetType());
        project.setCutOffDateForShowInterest(projectDTO.getCutOffDateForShowInterest());
        project.setDescription(projectDTO.getDescription());
        project.setEstimatedBudget(projectDTO.getEstimatedBudget());
        project.setEstimatedStartTime(projectDTO.getEstimatedStartTime());
        project.setEstimatedTime(projectDTO.getEstimatedTime());
        project.setFilesPath(projectDTO.getFilesPath());
        project.setLocationType(projectDTO.getLocationType());
        project.setTitle(projectDTO.getTitle());
        project.setSkill(projectDTO.getSkill());
        project.setPreferenceCountry(projectDTO.getPrefCountry());
        project.setNumberOfResources(projectDTO.getNumberOfResources());
        project.setStatus(ProjectStatusEnum.INITIAL.value());
        project.setGenericDetails(getGenericDetails());
        project.setId(projectDTO.getId());
        project.setProjectAddresses(projectAddressSet);
        return project;
    }

    public static List<ProjectDTO> convertProjectEntityToProjectDto(final List<Project> projects) {
        return projects.stream()
                .map(project -> {
                    ProjectDTO projectDTO = new ProjectDTO();
                    projectDTO.setId(project.getId());
                    projectDTO.setHuid(project.getHirer().getHuid());
                    projectDTO.setHirerName(project.getHirer().getName());

                    Set<ProjectAddress> projectAddresses = project.getProjectAddresses();
                    projectAddresses.stream().map(projectAddress -> {
                        projectDTO.setAddressLine(projectAddress.getAddress1());
                        projectDTO.setCity(projectAddress.getCity());
                        projectDTO.setState(projectAddress.getState());
                        projectDTO.setCountry(projectAddress.getCountry());
                        projectDTO.setZipCode(projectAddress.getZipCode());
                        return projectDTO;
                    });

                    projectDTO.setNumberOfResources(project.getNumberOfVacancies());
                    projectDTO.setSubSkills(Arrays.asList(project.getSubSkills().split(",")));
                    projectDTO.setBudgetType(project.getBudgetType());
                    projectDTO.setCutOffDateForShowInterest(project.getCutOffDateForShowInterest());
                    projectDTO.setDescription(project.getDescription());
                    projectDTO.setEstimatedBudget(project.getEstimatedBudget());
                    projectDTO.setEstimatedStartTime(project.getEstimatedStartTime());
                    projectDTO.setEstimatedTime(project.getEstimatedTime());
                    projectDTO.setFilesPath(project.getFilesPath());
                    projectDTO.setLocationType(project.getLocationType());
                    projectDTO.setTitle(project.getTitle());
                    projectDTO.setSkill(project.getSkill());
                    projectDTO.setPrefCountry(project.getPreferenceCountry());
                    return projectDTO;
                })
                .collect(Collectors.toList());
    }

    //************************************************************************
    // Private method(s)
    //************************************************************************

    private static GenericDetails getGenericDetails() {
        GenericDetails genericDetails = new GenericDetails();
        genericDetails.setCreatedBy("System");
        genericDetails.setCreatedTime(currentDateInStringFormat());
        return genericDetails;
    }

    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }

}
