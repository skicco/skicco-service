package com.skicco.spring.converter;

import com.skicco.spring.dto.*;
import com.skicco.spring.entity.*;
import com.skicco.spring.util.Utility;

import java.util.*;

/**
 * @author KRISHNA PANDU RANGA
 */
public class UserDetailsConverter {

    //************************************************************************
    // Public method(s)
    //************************************************************************

    public static Map<String, Object> convertUserDetailsDtoToEntity(final UserDetailsRequestDTO userDetailsRequestDTO) {
        Map<String, Object> userDetailsMap = new HashMap<>();
        GenericDetails genericDetails = buildGenericDetails(userDetailsRequestDTO);
        UserAddress userAddress = convertUserDetailsDtoToUserAddressEntity(userDetailsRequestDTO);
        userAddress.setGenericDetails(genericDetails);

        UserDetails userDetails = convertUserDetailsDtoToUserDetailsEntity(userDetailsRequestDTO);
        userDetails.setGenericDetails(genericDetails);

        UserEducation userEducation = convertUserDetailsDtoToUserEducationEntity(userDetailsRequestDTO);
        userEducation.setGenericDetails(genericDetails);

        UserPreferences userPreferences = convertUserDetailsDtoToUserPreferences(userDetailsRequestDTO);
        userPreferences.setGenericDetails(genericDetails);

        userDetailsMap.put("uuid", userDetailsRequestDTO.getUuid());
        userDetailsMap.put("name", userDetailsRequestDTO.getFirstName() + " " + userDetailsRequestDTO.getLastName());
        userDetailsMap.put("userAddress", userAddress);
        userDetailsMap.put("userDetails", userDetails);
        userDetailsMap.put("userEducation", userEducation);
        userDetailsMap.put("userPreferences", userPreferences);

        return userDetailsMap;
    }

    public static UserDashBoardDetailsDTO convertUserEntityToUserDashBoardSetailsDTO(User user) {
        UserDashBoardDetailsDTO userDashBoardDetailsDTO = new UserDashBoardDetailsDTO();

        userDashBoardDetailsDTO.setUuid(user.getUuid());
        userDashBoardDetailsDTO.setName(user.getName());
        userDashBoardDetailsDTO.setUserDetails(convertUserEntityToUserDetailsDto(user));
        Set<UserAddressDTO> userAddressDTOS = new HashSet<>();
        Set<UserEducationDTO> userEducationDTOS = new HashSet<>();
        for (UserAddress userAddress : user.getUserAddresses()
                ) {
            userAddressDTOS.add(convertUserEntityToUserAddressDTO(user));
        }
        for (UserEducation userEducation : user.getUserEducations()
                ) {
            userEducationDTOS.add(convertUserEntityToUserEducationDTO(user));
        }
        userDashBoardDetailsDTO.setUserAddress(userAddressDTOS);
        userDashBoardDetailsDTO.setUserEducation(userEducationDTOS);
        userDashBoardDetailsDTO.setUserPreferences(convertUserEntityToUserPreferancesDTO(user));
        userDashBoardDetailsDTO.setUserSkill(convertUserEntityToUserSkillDTO(user));
        return userDashBoardDetailsDTO;
    }

    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private static UserAddress convertUserDetailsDtoToUserAddressEntity(final UserDetailsRequestDTO userDetailsRequestDTO) {
        UserAddress userAddress = new UserAddress();
        userAddress.setDoorNumber(userDetailsRequestDTO.getDoorNumber());
        userAddress.setCity(userDetailsRequestDTO.getCity());
        userAddress.setState(userDetailsRequestDTO.getState());
        userAddress.setZipCode(userDetailsRequestDTO.getZipCode());
        userAddress.setCountry(userDetailsRequestDTO.getCountry());
        return userAddress;
    }

    private static UserDetails convertUserDetailsDtoToUserDetailsEntity(final UserDetailsRequestDTO userDetailsRequestDTO) {
        UserDetails userDetails = new UserDetails();
        userDetails.setFirstName(userDetailsRequestDTO.getFirstName());
        userDetails.setLastName(userDetailsRequestDTO.getLastName());
        userDetails.setAadharNumber(userDetailsRequestDTO.getAadharNum());
        userDetails.setMobileNumber(userDetailsRequestDTO.getMobileNumber());
        userDetails.setIntroduction(userDetailsRequestDTO.getIntroduction());
        return userDetails;
    }

    private static UserEducation convertUserDetailsDtoToUserEducationEntity(final UserDetailsRequestDTO userDetailsRequestDTO) {
        UserEducation userEducation = new UserEducation();
        userEducation.setEducationQualification(userDetailsRequestDTO.getEducationQualification());
        userEducation.setEducationSpecialization(userDetailsRequestDTO.getEducationSpecialization());
        userEducation.setCollegeName(userDetailsRequestDTO.getCollegeName());
        userEducation.setYearOfCompletion(userDetailsRequestDTO.getYearOfCompletion());
        return userEducation;
    }

    private static UserPreferences convertUserDetailsDtoToUserPreferences(final UserDetailsRequestDTO userDetailsRequestDTO) {
        UserPreferences userPreferences = new UserPreferences();
        userPreferences.setNoOfHours(userDetailsRequestDTO.getNoOfHours());
        userPreferences.setDistanceTravel(userDetailsRequestDTO.getDistanceTravel());
        userPreferences.setFromTime(userDetailsRequestDTO.getFromTime());
        userPreferences.setToTime(userDetailsRequestDTO.getToTime());
        return userPreferences;
    }

    private static UserDetailsDTO convertUserEntityToUserDetailsDto(User user) {
        UserDetailsDTO userDetailsDTO = new UserDetailsDTO();
        user.getUserDetails().forEach(response -> {
            userDetailsDTO.setFirstName(response.getFirstName());
            userDetailsDTO.setLastName(response.getLastName());
            userDetailsDTO.setIntroduction(response.getIntroduction());
            userDetailsDTO.setMobileNumber(response.getMobileNumber());
            userDetailsDTO.setAadharNumber(response.getAadharNumber());
        });
        return userDetailsDTO;
    }

    private static String convertUserEntityToUserSkillDTO(User user) {
        //UserSkillDTO userSkillDTO = new UserSkillDTO();
         String userSkills = null;
        for (UserSkill response : user.getUserSkills()) {
            userSkills = response.getSkillDetails();
        }

        return userSkills;
    }

    private static UserAddressDTO convertUserEntityToUserAddressDTO(User user) {
        UserAddressDTO userAddressDTO = new UserAddressDTO();

        user.getUserAddresses().forEach(response -> {
            userAddressDTO.setZipCode(response.getZipCode());
            userAddressDTO.setCity(response.getCity());
            userAddressDTO.setDoorNumber(response.getDoorNumber());
            userAddressDTO.setState(response.getState());
            userAddressDTO.setCountry(response.getCountry());
        });
        return userAddressDTO;
    }

    private static UserEducationDTO convertUserEntityToUserEducationDTO(User user) {
        UserEducationDTO userEducationDTO = new UserEducationDTO();

        user.getUserEducations().forEach(response -> {
            userEducationDTO.setCollegeName(response.getCollegeName());
            userEducationDTO.setEducationQualification(response.getEducationQualification());
            userEducationDTO.setEducationSpecialization(response.getEducationSpecialization());
            userEducationDTO.setYearOfCompletion(response.getYearOfCompletion());
        });
        return userEducationDTO;
    }

    public static UserPreferancesDTO convertUserEntityToUserPreferancesDTO(User user) {
        UserPreferancesDTO userPreferancesDTO = new UserPreferancesDTO();

        user.getUserPreferences().forEach(response -> {
            userPreferancesDTO.setNoOfHours(response.getNoOfHours());
            userPreferancesDTO.setDistanceTravel(response.getDistanceTravel());
            userPreferancesDTO.setFromTime(response.getFromTime());
            userPreferancesDTO.setToTime(response.getToTime());
        });
        return userPreferancesDTO;
    }
    // private static

    private static GenericDetails buildGenericDetails(final UserDetailsRequestDTO userDetailsRequestDTO) {
        GenericDetails genericDetails = new GenericDetails();
        String name = userDetailsRequestDTO.getFirstName() + " " + userDetailsRequestDTO.getLastName();
        genericDetails.setCreatedBy(name);
        genericDetails.setCreatedTime(currentDateInStringFormat());
        return genericDetails;
    }

    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }
}
