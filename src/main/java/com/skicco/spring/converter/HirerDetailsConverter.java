package com.skicco.spring.converter;

import com.skicco.spring.dto.HirerDetailsDTO;
import com.skicco.spring.entity.Company;
import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.Hirer;
import com.skicco.spring.entity.HirerAddress;
import com.skicco.spring.util.Utility;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author SriHarsha
 */

public class HirerDetailsConverter {

    private static final String CLIENT_CORPORATE="Corporate";

    public static Map<String, Object> convertUserDetailsDtoToEntity(final HirerDetailsDTO hirerDetailsDTO) {
        Map<String, Object> hirerDetailsMap = new HashMap<>();
        GenericDetails genericDetails = buildGenericDetails(hirerDetailsDTO);
        HirerAddress hirerAddress = convertHirerDetailsDtoToHirerAddressEntity(hirerDetailsDTO);
        hirerAddress.setGenericDetails(genericDetails);

        if(hirerDetailsDTO.getClientType().equalsIgnoreCase(CLIENT_CORPORATE)) {
            Company company = convertHirerDetailsDtoToCompanyEntity(hirerDetailsDTO);
            company.setGenericDetails(genericDetails);
            hirerDetailsMap.put("company", company);
        }
        Hirer hirer = convertHirerDetailsDtoToHirerEntity(hirerDetailsDTO);
        hirer.setGenericDetails(genericDetails);

        hirerDetailsMap.put("huid", hirerDetailsDTO.getHuid());
        hirerDetailsMap.put("name", hirerDetailsDTO.getFirstName() + " " + hirerDetailsDTO.getLastName());
        hirerDetailsMap.put("hirerAddress", hirerAddress);
        hirerDetailsMap.put("hirer",hirer);
        return hirerDetailsMap;
    }

    private static Hirer convertHirerDetailsDtoToHirerEntity(HirerDetailsDTO hirerDetailsDTO) {

        Hirer hirer = new Hirer();
        hirer.setFirstName(hirerDetailsDTO.getFirstName());
        hirer.setLastName(hirerDetailsDTO.getLastName());
        hirer.setClientType(hirerDetailsDTO.getClientType());
        hirer.setMobileNumber(hirerDetailsDTO.getMobileNumber());
        hirer.setPosition(hirerDetailsDTO.getPosition());
        hirer.setIsHirerVerified(hirerDetailsDTO.getIsHirerVerified());
        return hirer;
    }

    private static Company convertHirerDetailsDtoToCompanyEntity(HirerDetailsDTO hirerDetailsDTO) {

        Company company = new Company();
        company.setName(hirerDetailsDTO.getCompanyName());
        return company;
    }

    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private static HirerAddress convertHirerDetailsDtoToHirerAddressEntity(final HirerDetailsDTO hirerDetailsDTO) {
        HirerAddress hirerAddress = new HirerAddress();
        hirerAddress.setDoorNumber(hirerDetailsDTO.getHirerAddressDTO().getAddr());
        hirerAddress.setCity(hirerDetailsDTO.getHirerAddressDTO().getCity());
        hirerAddress.setState(hirerDetailsDTO.getHirerAddressDTO().getState());
        hirerAddress.setZipCode(hirerDetailsDTO.getHirerAddressDTO().getZipCode());
        hirerAddress.setCountry(hirerDetailsDTO.getHirerAddressDTO().getCountry());
        return hirerAddress;
    }

    // private static

    private static GenericDetails buildGenericDetails(final HirerDetailsDTO hirerDetailsDTO) {
        GenericDetails genericDetails = new GenericDetails();
        String name = hirerDetailsDTO.getFirstName() + " " + hirerDetailsDTO.getLastName();
        genericDetails.setCreatedBy(name);
        genericDetails.setCreatedTime(currentDateInStringFormat());
        return genericDetails;
    }
    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }

}
