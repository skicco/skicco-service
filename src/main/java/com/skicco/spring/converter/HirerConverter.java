package com.skicco.spring.converter;


import com.skicco.spring.dto.HirerDTO;
import com.skicco.spring.dto.UserDTO;
import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.Hirer;
import com.skicco.spring.entity.User;
import com.skicco.spring.util.Utility;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.UUID;

/**
 * @author SriHarsha
 *
 */
public class HirerConverter {


    //************************************************************************
    // Constants
    //************************************************************************

    private static final String NOT_AUTHORIZED = "Not Authorized";
    private static final String SYSTEM = "System";

    //************************************************************************
    // Public method(s)
    //************************************************************************

    public static Hirer convertHirerDtoToHirerEntity(final HirerDTO hirerDTO) {
        GenericDetails genericDetails = new GenericDetails();
        Hirer hirer = new Hirer();
        hirer.setEmail(hirerDTO.getEmailId());
        hirer.setPassword(hirerDTO.getPwd());
        hirer.setHuid(
                (UUID.randomUUID().toString() + "-" + StringUtils.lowerCase(Utility.base64Encode(hirer.getEmail())))
                        .replaceAll("=", ""));
        hirer.setPassword(Utility.base64Encode(hirer.getPassword()));
        hirer.setStatus(NOT_AUTHORIZED);

        genericDetails.setCreatedBy(SYSTEM);
        genericDetails.setCreatedTime(currentDateInStringFormat());
        hirer.setGenericDetails(genericDetails);
        return hirer;
    }

    public static Hirer convertHirerDtoToHirerForLogin(final HirerDTO hirerDTO) {
        Hirer hirer = new Hirer();
        hirer.setEmail(hirerDTO.getEmailId());
        hirer.setPassword(hirerDTO.getPwd());
        return  hirer;

    }


    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }


}
