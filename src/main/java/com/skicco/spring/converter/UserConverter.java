package com.skicco.spring.converter;

import com.skicco.spring.dto.UserDTO;
import com.skicco.spring.dto.UserDashBoardDetailsDTO;
import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.User;
import com.skicco.spring.util.Utility;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.UUID;

/**
 * @author KRISHNA PANDU RANGA
 */
public class UserConverter {

    //************************************************************************
    // Constants
    //************************************************************************

    private static final String NOT_AUTHORIZED = "Not Authorized";
    private static final String SYSTEM = "System";

    //************************************************************************
    // Public method(s)
    //************************************************************************

    public static User convertUserDtoToEntity(final UserDTO userDTO) {
        GenericDetails genericDetails = new GenericDetails();
        User user = new User();
        user.setEmailId(userDTO.getEmailId());
        user.setPwd(userDTO.getPwd());
        user.setUuid(
                (UUID.randomUUID().toString() + "-" + StringUtils.lowerCase(Utility.base64Encode(user.getEmailId())))
                        .replaceAll("=", ""));
        user.setPwd(Utility.base64Encode(user.getPwd()));
        user.setStatus(NOT_AUTHORIZED);

        genericDetails.setCreatedBy(SYSTEM);
        genericDetails.setCreatedTime(currentDateInStringFormat());
        user.setGenericDetails(genericDetails);
        return user;
    }



   /* public static UserDashBoardDetailsDTO convertUserEntityToUserDashBoardDetailsDto(final User user) {
        UserDashBoardDetailsDTO userDashBoardDetailsDTO = new UserDashBoardDetailsDTO();
        userDashBoardDetailsDTO.setName(user.getName());
        userDashBoardDetailsDTO.setUuid(user.getUuid());
        userDashBoardDetailsDTO.setUserDetails(user.getUserDetails());
        userDashBoardDetailsDTO.setUserSkill(user.getUserSkills());
        userDashBoardDetailsDTO.setUserAddress(user.getUserAddresses());
        userDashBoardDetailsDTO.setUserEducation(user.getUserEducations());
        userDashBoardDetailsDTO.setUserPreferences(user.getUserPreferences());
        return userDashBoardDetailsDTO;
    }*/
    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }

}
