package com.skicco.spring.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.skicco.spring.dto.SubSkillDTO;
import com.skicco.spring.dto.UserSkillDTO;
import com.skicco.spring.entity.GenericDetails;
import com.skicco.spring.entity.UserSkill;
import com.skicco.spring.entity.UserSkillDetails;
import com.skicco.spring.util.Utility;

import java.util.*;

/**
 * @author KRISHNA PANDU RANGA
 */
public class UserSkillConverter {

    //************************************************************************
    // Constants
    //************************************************************************

    private static final String SYSTEM = "System";

    //************************************************************************
    // Public method(s)
    //************************************************************************

    public static UserSkill convertUserSkillDtoToUserSkillEntity(UserSkillDTO userSkillDTO) {
        ObjectMapper objectMapper = new ObjectMapper();
        UserSkill userSkill = new UserSkill();
        Map<String, List<SubSkillDTO>> skillsMap = buildSkillDetails(userSkillDTO);
        String skillDetails = null;
        try {
            skillDetails = objectMapper.writeValueAsString(skillsMap);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        userSkill.setSkillDetails(skillDetails);
        userSkill.setGenericDetails(getGenericDetailRecords());
        return userSkill;
    }

    //*********************************************************************************
    //Private Method(s)
    //*********************************************************************************

    private static Map<String, List<SubSkillDTO>> buildSkillDetails(UserSkillDTO userSkillDTO) {
        List<UserSkillDetails> userSkills = userSkillDTO.getUserSkills();
        Map<String, List<SubSkillDTO>> skillsMap = new HashMap<>();
        List<SubSkillDTO> subSkills = null;
        for (UserSkillDetails userSkill : userSkills) {
            String skill = userSkill.getSkill();
            SubSkillDTO subSkill = new SubSkillDTO(userSkill.getSubSkill(), userSkill.getRating());
            if (skillsMap.containsKey(skill)) {
                subSkills = skillsMap.get(skill);
            } else {
                subSkills = new LinkedList<>();
            }
            subSkills.add(subSkill);
            skillsMap.put(skill, subSkills);
        }
        return skillsMap;
    }

    private static GenericDetails getGenericDetailRecords() {
        GenericDetails genericDetails = new GenericDetails();
        genericDetails.setCreatedBy(SYSTEM);
        genericDetails.setCreatedTime(currentDateInStringFormat());
        return genericDetails;
    }

    private static String currentDateInStringFormat() {
        return Utility.convertDateToString(new Date());
    }
}
